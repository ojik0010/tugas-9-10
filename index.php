<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$Sheep = new Animal("shaun"); //animal diambil dr class kendaraan

echo "Name : $Sheep->name <br>";
echo "legs : $Sheep->legs <br>";
echo "cold blooded : $Sheep->cold_blooded <br><br>";

$kodok = new Frog("buduk");
$sungkong = new Ape("Kera sakti");

echo "Name : $kodok->name <br>";
echo "legs : $kodok->legs <br>";
echo "cold blooded : $kodok->cold_blooded <br>";
$kodok->jump(); //untuk function isinya diprint di function jump() file frog.php

echo "<br><br>";
echo "Name : $sungkong->name <br>";
echo "legs : $sungkong->legs <br>";
echo "cold blooded : $sungkong->cold_blooded <br>";
$sungkong->yell();
?>